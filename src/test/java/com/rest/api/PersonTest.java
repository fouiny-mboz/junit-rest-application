package com.rest.api;

import com.rest.api.model.Person;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class PersonTest {

    @Mock
    private Person person;


    @Mock
    private List listModed;

    @Captor
    ArgumentCaptor captor;


    @Before
    @Test
    public void setup(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void should_test_when_theReturn(){
        //Given
        String name = "Arnaud";
        Mockito.when(person.findName()).thenReturn(name);
        //When
        String actual = person.findName();
        //Then
        assertNotNull(actual);
        assertEquals(name, actual);
    }

    @Test
    public void should_test_verify(){
        //Given
        String name = "Arnaud";
        Mockito.when(person.findName()).thenReturn(name);
        //When
        String actual = person.findName();
        String actual2 = person.findName();
        //Then
        // Vérifie que le mock person a bien étét appelé par notre method findName()
        Mockito.verify(person, Mockito.times(2)).findName();
        Mockito.verify(person, Mockito.atLeastOnce()).findName();
        assertNotNull(actual);
        assertEquals(name, actual);
    }

    @Test
    public void should_test_spy(){
        // Créer un nouveau mock
        List<String> list = Mockito.spy(new ArrayList<String>());
        list.add("baba");
        list.add("toto");
        list.add("tati");
        assertEquals(3, list.size());
        Mockito.verify(list).add("toto");
        Mockito.verify(list).add("baba");

        Mockito.when(list.size()).thenReturn(23);
        assertEquals(23, list.size());
    }

    @Test
    public void shoul_test_spy2(){
        Person person1 = Mockito.spy(Person.class);
        String actual = person1.findName();

        assertEquals("Votre nom", actual);
        Mockito.verify(person1).findName();
    }

    @Test
    public void should_test_captor(){
        listModed.add("baba");
        Mockito.verify(listModed).add(captor.capture());
        assertEquals("baba", captor.getValue());
    }

}

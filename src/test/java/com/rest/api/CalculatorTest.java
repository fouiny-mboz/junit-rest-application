package com.rest.api;

import com.rest.api.calculator.Calculator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    Calculator calculator;

    @Before
    public void setUp(){
        calculator = new Calculator();
    }

    @Test
    public void testMultiply(){
        assertEquals(20, calculator.multiply(5, 4));
        assertEquals(25, calculator.multiply(5, 5));
    }

    @Test
    public void testDivide(){
        assertEquals(3, calculator.divide(6, 2));
    }
}
